#!/bin/bash

for f in app/*/*.yaml
do
    sed -i '' 's/namespace: staging/namespace: production/g' $f
done